from django.urls import path
from.views import cargaFactura,editaFactura,borraFactura
urlpatterns = [
    path('factura/',cargaFactura,name="factura"),
    path('editafactura/<int:pk>/',editaFactura.as_view(),name="editaFactura"),
    path('borrafactura/<int:pk>/',borraFactura.as_view(),name="borraFactura"),
]