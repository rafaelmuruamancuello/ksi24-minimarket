from django.shortcuts import render,reverse,redirect
from.forms import formFacturas
from.models import facturas
from django.views.generic import UpdateView,DeleteView
# Create your views here.
def cargaFactura(request):
    factura=facturas.objects.all()
    if request.method=='POST':
        form=formFacturas(request.POST)
        if form.is_valid():
            form.save()
        return render(request,'facturas/cargafactura.html',{'form':form,'factura':factura}) 
    else:
        form=formFacturas
        return render(request,'facturas/cargafactura.html',{'form':form,'factura':factura})
class editaFactura(UpdateView):
    model=facturas
    form_class=formFacturas
    template_name='facturas/editafactura.html'
    def get_success_url(self):
        return reverse ('factura')
class borraFactura(DeleteView):
    model=facturas
    template_name='facturas/borrafactura.html'
    def get_success_url(self):
        return reverse ('factura')
