from django import forms
from.models import facturas

class formFacturas(forms.ModelForm):
    class Meta:
        model=facturas
        fields=[
            'tipoFactura',
            'detalle',
            'fechaPago',
            'valor',
        ]
        labels={
            'tipoFactura':'Tipo de gasto',
            'detalle':'Detalle de gasto',
            'fechaPago':'Fecha del pago',
            'valor':'Monto del pago',
        }
        widgets={
            'tipoFactura':forms.TextInput(attrs={'class':'form-control' ,'id':'tipoFactura' }),
            'detalle':forms.TextInput(attrs={'class':'form-control' ,'id':'detalle'}),
            'fechaPago':forms.DateInput(attrs={'class':'form-control' ,'id':'fechaPago'}),  
            'valor':forms.TextInput(attrs={'class':'form-control' ,'id':'valor'}),
        }