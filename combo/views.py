from django.shortcuts import render,HttpResponse,redirect,reverse
from .models import Producto,Combo,DetalleComboProductos
import json
from django.db.models import Q
# Create your views here.



def templateCombo(request):
  return render(request,'combo/combos.html')

def listarCombos(request):
  if request.method == 'GET':
    q = request.GET.get('q', '')
    eventos = Combo.objects.filter(nombre__icontains=q)
    return render(request,'combo/listaCombos.html',{'eventos':eventos})

def verCombo(request,pk):
  combo=DetalleComboProductos.objects.filter(combos__id=pk)
  return render(request,'combo/muestracombo.html',{'combo':combo})



def EliminaCombo(request,pk):
  combos=Combo.objects.filter(id=pk)
  combos.delete()
  return render(request,'combo/elimina.html')











def buscarProducto(request):
  if request.method=='POST':
    valor=request.POST['codigoProducto'] 
    productos=Producto.objects.filter( Q(codigo=valor) | Q(nombre__icontains=valor)).values('id','codigo','nombre','precioVenta','stock','pesaje','tipoProducto')
    return HttpResponse( json.dumps( list(productos)), content_type='application/json' )  
  else:
      return HttpResponse("errorrr")


def generarCombo(request):
  codigoCombo = request.POST.get('codigo')
  nombreCombo = request.POST.get('nombre')
  cantidadTotalCombo = request.POST.get('cantidadTotalCombo')
  precioCombo = request.POST.get('precioTotal')
  Combo.objects.create(codigo=codigoCombo,nombre=nombreCombo,cantidad=cantidadTotalCombo,precioVenta=precioCombo,)
  ultimoIdCombo=Combo.objects.last()
  productos = request.POST.getlist('idProducto[]')
  cantidadesIndividuales = request.POST.getlist('cantidad[]')
  diccionarioProductos = {} 
  diccionarioCantidadProductos = {} 
 
  for posicion, valorId in enumerate(productos):
    diccionarioProductos[posicion] = valorId
     
  for posicion, valorCantidad in enumerate(cantidadesIndividuales):
    diccionarioCantidadProductos[posicion] = valorCantidad
  #print(diccionarioProductos)
  #print(diccionarioCantidadProductos)
  for indice, b in enumerate(diccionarioProductos):
    idProducto=diccionarioProductos[indice]
    cantidad=diccionarioCantidadProductos[indice]
    if Producto.objects.filter(pk=idProducto).exists():
      instancia = Producto.objects.get(pk =idProducto)#Bien Intancia requerida
      DetalleComboProductos.objects.create(combos=ultimoIdCombo,productos=instancia,cantidad=cantidad)
    else:
      print("Ocurrio un error en la busqueda del id Prodcuto")
  return HttpResponse(ultimoIdCombo)

