from django.urls import path
from.views import templateCombo,buscarProducto,generarCombo,listarCombos,verCombo,EliminaCombo
urlpatterns = [
    path('combos/',templateCombo,name="combos"),
    path('buscarProducto/',buscarProducto,name="buscarProductos"),
    path('generarCombo/',generarCombo,name="Combo"),
    path('listaCombos/',listarCombos,name="ComboListados"),
    path('productosCombo/<int:pk>/',verCombo,name="ListadosCombos"),
    path('eliminaCombo/<int:pk>/',EliminaCombo,name="EliminaCombos"),
]