from django.db import models
from core.models import Producto
from ventas.models import Venta,DetalleVenta
# Create your models here.
class Combo (models.Model):
    codigo=models.CharField(max_length=50,blank=True,null=True,verbose_name="Codigo del combo")
    nombre=models.CharField(max_length=50,blank=True,null=True,verbose_name="Nomre del combo")
    precioVenta=models.FloatField(blank=True, null=True, verbose_name="Precio total del combo")
    cantidad=models.IntegerField(blank=True, null=True, verbose_name="Cantidad total de productos combo")
    def __int__(self):
        return self.id

class DetalleComboProductos(models.Model):
    combos=models.ForeignKey(Combo,on_delete=models.CASCADE)
    productos=models.ForeignKey(Producto,on_delete=models.CASCADE)
    cantidad=models.IntegerField(blank=True, null=True, verbose_name="Cantidad de producto en el combo")

class DetalleCombo(models.Model):
    ventas=models.ForeignKey(Venta,on_delete=models.CASCADE)
    combos=models.ForeignKey(Combo,on_delete=models.CASCADE)
    precioVenta=models.FloatField(blank=True, null=True, verbose_name="Precio total del combo")
