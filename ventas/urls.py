from django.urls import path
from ventas import views




#aca genero las urls que va a tener la app core de nuestro proyecto 
urlpatterns = [
    path('muestraVenta/',views.muestraVenta, name="muestraVentas"),
    path('ventas/',views.renderizaVenta, name="ventas"),
    path('ventaTerminada/',views.terminarLaVenta, name="TerminarVenta"),
    path('buscarCodigoProducto/',views.BuscarCodigoProducto, name="buscarCProducto"),

]