from django.shortcuts import render,reverse,HttpResponse
from .models import Venta,DetalleVenta,Producto
from.forms import formVenta,formDetalle
import json
from datetime import date
from datetime import datetime
from django.http import JsonResponse
from django.db.models import F
from django.db.models import Count
from combo.models import Combo,DetalleCombo,DetalleComboProductos
from django.db.models import Q
# Create your views here.







##########################################################################################


def muestraVenta(request):
  ventas=Venta.objects.all()
  return render(request,"ventas/muestraventa.html",{'ventas':ventas})


def renderizaVenta(request):
  return render(request,"ventas/ventas.html")



def terminarLaVenta(request):
  usuario = request.POST.get('usuario')
  precioTotal = request.POST.get('precioVentaTotal')
  tipoDePago = request.POST.get('tipoPago')
  print(tipoDePago)
  if tipoDePago == None:
    tipoDePago=False# efectivo
  else:
    tipoDePago=True #debito
  Venta.objects.create(usuario=usuario,totalVenta=precioTotal,tipoPago=tipoDePago)
  ultimoIdVenta=Venta.objects.last()
  productos = request.POST.getlist('producto[]')
  precioProductos = request.POST.getlist('precioUnitario[]')
  pesajes = request.POST.getlist('pesaje[]')
  #print(precioProductos)
  #print(productos)

  response_data = {} # Productos
  response_data2 = {} #Precio de productos
  response_data3 = {} #Pesaje de productos
  for i, v in enumerate(productos):
   # print(i, v)
    response_data[i] = v
     
  for a, b in enumerate(precioProductos):
    #print(a, b)
    response_data2[a] = b

  for c, d in enumerate(pesajes):
   # print(c, d)
    response_data3[c] = d
      
  #print(response_data)
  #print(response_data2)
  #print(response_data3)
  for indice, b in enumerate(response_data):
    producto=response_data[indice]
    precio=response_data2[indice]
    pesaje=response_data3[indice]
    #print(pesaje)
    if Producto.objects.filter(codigo=producto).exists():
      instancia = Producto.objects.get(codigo =producto)#Bien Intancia requerida
      DetalleVenta.objects.create(ventas=ultimoIdVenta,producto=instancia,precioProducto=precio)
      if int(pesaje) > 0:
        pesajes=Producto.objects.filter(codigo=producto).values('pesaje')
        for pesajeIndividual in pesajes:
          pesajeBD=pesajeIndividual['pesaje']
        if pesajeBD<=0:
          Producto.objects.filter(codigo=producto).update(pesaje=0)
        else:
          Producto.objects.filter(codigo=producto).update(pesaje=F('pesaje') - pesaje)
      else:
        stocks=Producto.objects.filter(codigo=producto).values('stock')
        for stockIndividual in stocks:
          stock=stockIndividual['stock']
        if stock<=0:
          Producto.objects.filter(codigo=producto).update(stock=0)
        else:
          Producto.objects.filter(codigo=producto).update(stock=F('stock') - 1)#disminuye stock        
    else:
      instanciaCombo = Combo.objects.get(codigo =producto)#Bien Intancia requerida
      #DetalleVenta.objects.create(ventas=ultimoIdVenta,producto=instanciaCombo,precioProducto=precio)
      DetalleCombo.objects.create(ventas=ultimoIdVenta,combos=instanciaCombo,precioVenta=precio)
      ProductosDelCombo = DetalleComboProductos.objects.filter(combos =instanciaCombo).values('combos','productos','cantidad')#Bien Intancia requerida
      print(ProductosDelCombo)
      for i in ProductosDelCombo:
        print(i['productos'])
        idProducto=i['productos']
        cantidad=i['cantidad']
        Producto.objects.filter(pk=idProducto).update(stock=F('stock') - cantidad)#disminuye stock de acuerdo la cantidad de los productos agregado al combo

    
  
      
    
  #  DetalleVenta.objects.create(ventas=ultimoIdVenta,producto=instancia,precioProducto=precio) 

    
  #for (producto, precio) in d.items():
   # instancia = Producto.objects.get(codigo =producto)#Bien
    #b=DetalleVenta(ventas=ultimoIdVenta,producto=instancia,precioProducto=precio)
    #b.save()
   # DetalleVenta.objects.create(ventas=ultimoIdVenta,producto=instancia,precioProducto=precio) 
   # print(instancia)
    #print(precio)

  return HttpResponse(ultimoIdVenta)


    


def BuscarCodigoProducto(request):
  if request.method=='POST':
    valor=request.POST['codigoProducto'] 
    productos=Producto.objects.filter(Q(codigo=valor) | Q(nombre=valor)).values('id','codigo','nombre','precioVenta','stock','pesaje','tipoProducto')
    objetoEncontrado=productos.count()
    if objetoEncontrado==0:
      productos=Combo.objects.filter(Q(codigo=valor) | Q(nombre__icontains=valor)).values('id','codigo','nombre','precioVenta')
      return HttpResponse( json.dumps( list(productos)), content_type='application/json' )
    else:
      return HttpResponse( json.dumps( list(productos)), content_type='application/json' )  
      
  else:
      return HttpResponse("errorrr")